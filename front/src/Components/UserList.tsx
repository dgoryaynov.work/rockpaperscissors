import React, { FC } from "react";


export const UserList: FC<{ name: string, score: number }[]> = (list) => {
  return (
    <div>
      {list.map(p => <div>{p.name}<span>{p.score}</span></div>)}
    </div>
  )
};
UserList.displayName = 'ReUserListgistration';