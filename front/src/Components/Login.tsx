import React, { FC } from "react";
import styles from "./Login.module.css";

export const Login: FC<{}> = () => {
  return (
    <div className={styles.login}>Login</div>
  )
};
Login.displayName = 'Login';